var gulp = require('gulp');
var config = require('../config');

gulp.task('generation', gulp.series(
  'clean',
  gulp.parallel(
    'postcss',
    'pug',
  ),
  // 'webpack',
  'copy',
  'list-pages',
));

gulp.task('build', gulp.series(function(done) {
  config.setEnv('production');
  config.logEnv();
  done();
}, 'generation'));

gulp.task('build:dev', gulp.series(function(done) {
  config.setEnv('development');
  config.logEnv();
  done();
}, 'generation'));
